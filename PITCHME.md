@audio[Intro](assets/audio/slide1.mp3)
### **From Notebook to Production**
 
 <br>
 <br>    
### Sean P. Carey
### Regis University
---

@audio[Slide2](assets/audio/2_0.mp3)

@snap[north span-95]
### **Deploy a Machine Learning Model as a Scalable API**
<br>
@snapend

@snap[east span-55 text-center]
<br><br><br><br><br>
@ul[list-square-bullets list-spaced-bullets ](true)
- PyTest @note[](assets/audio/2_1.mp3)
- Flask @note[](assets/audio/2_2.mp3)
- Docker and Kubernetes @note[](assets/audio/2_3.mp3)
- GitLab for CICD @note[](assets/audio/2_4.mp3)
@ulend

@snapend



@snap[west span-30]
<br>
<br>
<br>
![](assets/img/k8s.png)

@snapend

---

@audio[slide4](assets/audio/3_0.mp3)

@snap[north span-100].
### Collected 17-18 January, 2020
*1,350,306 Tweets* 

@snapend

@snap[midpoint span-100 ]
<br><br>
![](assets/img/EDA/collection.svg)
@snapend

---

@audio[slide4](assets/audio/4_0.mp3)

@snap[ text-center ]
![width=350](assets/img/fastai.png)
<br>
![width=350](assets/img/epochs.png)
<br>
![width=350](assets/img/EDA/metrics.png)
@snapend
---
@audio[](assets/audio/5_0.mp3)

@snap[west text-left span-55]
@tweet[https://twitter.com/SenSanders/status/1228778506928164864]
@snapend

@snap[east span-40 text-center text-yellow]
### **Prediction**
 **94% Liberal<br><br>**
@snapend

---
@audio[](assets/audio/6_0.mp3)

@snap[east span-40 code-wrap]

```json
# Request
{
text: "The world's richest 1% have over twice the wealth of 6.9 billion people. The planet will not be secure or peaceful when so few haveso much and so many have so little.."
}
``` 
<br><br><br>
@snapend

@snap[north fragment](assets/audio/6_1.mp3)
<br><br><br>
@fa[arrow-left]
@snapend


@snap[north-west text-center span-40 fragment](assets/audio/6_2.mp3)
<br>
![](assets/img/flask.png) 
<br><br>
@snapend

@snap[west text-center fragment](assets/audio/6_3.mp3)
<br><br><br>
@fa[arrow-down] 
<br>
```json
# Response
{
percent_in_class: 94.0,
predicted_class: "Liberal"
}
```
@snapend

---
@audio[](assets/audio/7_0.mp3)

@snap[north-west span-35]
<br>
### *Define App Behavior*
@snapend

@snap[east span-60 fragment](assets/audio/7_1.mp3)
![](app_home.png) 
![](api_good.png) 
![](bad_request1.png)
![](bad_request2.png)
@snapend
@snap[west span-35 text-center fragment](assets/audio/7_5.mp3)
<br><br><br><br>
![](assets/img/pytest.png)
@snapend

---
@audio[](assets/audio/8_0.mp3)

@snap[north-west span-35 fragment](assets/audio/8_1.mp3)
@box[bg-green text-white ](1. Write Tests# Write Tests that represent your functionality)
@snapend

@snap[north-east span-35 fragment](assets/audio/8_2.mp3)
@box[bg-orange text-white ](2. Code#Write Code for only one test at a time)
@snapend

@snap[south-east span-35 fragment](assets/audio/8_3.mp3)
@box[bg-pink text-white ](3. Test#Run the test suite from command line)
@snapend

@snap[south-west span-35 fragment](assets/audio/8_4.mp3)
@box[bg-blue text-white ](4. Repeat#If your test passes, move to next test until no tests fail)
@snapend

@snap[midpoint]
@fa[sync  fa-2x]
@snapend
---

@audio[](assets/audio/9_0.mp3)

@snap[midpoint span-100 ]
![](assets/img/basic_app.png)
@snapend

@snap[south span-100]
### *App Running locally*
@snapend

---
@audio[](assets/audio/10_0.mp3)

@snap[midpoint span-100 ]
<br><br>
![](assets/img/docker_app.png)
<br><br>
@snapend

@snap[south span-100]
### **App running in a Local Docker Container**
@snapend


---

@audio[](assets/audio/11_0.mp3)

@snap[north span-95]
## Why are containers so cool?
@snapend

@snap[west text-left]
![width=200, height=150](assets/img/moby.png)
@snapend

@ul[list-no-bullets list-fade-fragments text-center]
<br><br>
- Consistent @note[](assets/audio/11_1.mp3)
- Portable  @note[](assets/audio/11_2.mp3)
- Lightweight @note[](assets/audio/11_3.mp3)
@ulend

@snap[south fragment](assets/audio/11_4.mp3)
## Orchestration  
@snapend 

@snap[east text-right]
![width=200, height=150](assets/img/k8s.png)
@snapend

---
@audio[](assets/audio/12_0.mp3)

@snap[midpoint]
![](assets/img/k8s_blank.png)
@snapend
---
@audio[](assets/audio/13_0.mp3)


@snap[midpoint]
![](assets/img/k8s_app_1.png)
@snapend
---
@audio[](assets/audio/14_0.mp3)


@snap[midpoint]
![](assets/img/k8s_app_2.png)
@snapend
---
@audio[](assets/audio/15_0.mp3)


@snap[midpoint]
![](assets/img/k8s_app_4.png)
@snapend
---
@audio[](assets/audio/16_0.mp3)


@snap[midpoint]
![](assets/img/k8s_app_1.png)
@snapend
---
@audio[](assets/audio/17_0.mp3)


@snap[midpoint]
![](assets/img/DO_K8S.jpeg)
@snapend
---

@audio[](assets/audio/18_0.mp3)
@snap[north-west text-center span-40]
Code
![](assets/img/flask.png)
@snapend

@snap[north-east text-center span-40 fragment](assets/audio/18_1.mp3)
Build
![width=200, height=150 ](assets/img/moby.png)
@snapend

@snap[south-west text-center span-40 fragment](assets/audio/18_2.mp3)
Test
![](assets/img/pytest.png)
@snapend

@snap[south-east text-center span-40 fragment](assets/audio/18_3.mp3)
Deploy
![width=200, height=150 ](assets/img/k8s.png)
@snapend

@snap[midpoint text-center span-40 fragment](assets/audio/18_4.mp3)
![](assets/img/CICD.png)
@snapend
---

@audio[](assets/audio/19_0.mp3)

@snap[midpoint text-center span-30 ]
![](assets/img/gitlab_cicd.png)
@snapend

@snap[south text-center span-60]
![](assets/img/pipe.png) 
@snapend

@snap[west text-center span-30 fragment](assets/audio/19_1.mp3)
Continuous <br>
Integration
![](assets/img/git.png)
@snapend

@snap[east text-center span-30 fragment](assets/audio/19_2.mp3)
Continuous <br>
Delivery
![](assets/img/cncf.png)
@snapend
---

@audio[](assets/audio/20_0.mp3)

@snap[midpoint text-center span-90 ]
## Data Science <br> and <br> DevOps
@snapend
